/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class VectorNumero implements IMatriz, IMatriz2{
    
    int numeros[];

    public VectorNumero() {
    }
    
    
    public VectorNumero(int n) throws Exception {
        if(n<=0)
            throw new Exception("No se puede crear el vector:");
        this.numeros=new int[n];
        
        this.crearNumeros(n);
        
    }
    
    private void crearNumeros(int n)
    {
        
     for(int i=0;i<n;i++)   
     {
         this.numeros[i]=i;
     }
    
    }

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {
        
        if(this.numeros==null)
            return ("Vector vacío");
        String msg="";
        for(int dato:this.numeros)
            msg+=dato+"\t";
        
        return msg;
        
    }

    @Override
    public int getSumaTotal() {
        int t=0;
        for(int dato:this.numeros)
           t+=dato;
        
        return t;
    }
    
    //metodo que me retorna la cantidad de veces que se repite n en el vector
    private int getRepeticiones(int n){
        int rep=0;
        for(int dato:this.numeros){
            if(dato==n){
                rep++;
            }
        }
        return rep;
    }

    @Override
    public int getMas_Se_Repite() {
        int maxRep=this.getRepeticiones(this.numeros[0]);
        int nMax=this.numeros[0];
        for(int dato:this.numeros){
            int rep=this.getRepeticiones(dato);
            if(rep>maxRep){
                nMax=dato;
                maxRep=rep;
            }
        }
        return nMax;
    }
    
    
}