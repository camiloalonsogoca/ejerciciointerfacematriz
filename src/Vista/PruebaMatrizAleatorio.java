/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.IMatriz;
import Interface.IMatriz2;
import Negocio.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class PruebaMatrizAleatorio {
 
    public static void main(String[] args) {
        
        try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas=lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols=lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini=lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin=lector.nextInt();
            
            MatrizEntero_Aleatoria myMatriz=new MatrizEntero_Aleatoria(filas, cols);
            myMatriz.crearMatriz(ini, fin);
            System.out.println("Mi matriz:"+myMatriz.toString());
            
            //Forma 2:POLIMORFISMO
            IMatriz2 matriz_o=myMatriz;
            System.out.println("Mas Repetido Forma 2: "+matriz_o.getMas_Se_Repite());
            
            //Forma 3:CASTING DE LA INTERFACE
            
            int num_rep=((IMatriz2)myMatriz).getMas_Se_Repite();
            System.out.println("Mas Repetido Forma 3: "+num_rep);
            
            Par_Numero par1=new Par_Numero(4,5);
            //Forma2:
            IMatriz2 i1=par1;
            System.out.println("Mas repetido forma2 (Par):"+i1.getMas_Se_Repite());
            
            VectorNumero vector1=new VectorNumero(5);
            //Forma2:
            IMatriz2 i2=vector1;
            System.out.println("Mas repetido forma2(Vector=:"+i2.getMas_Se_Repite());
            
            // Forma1:
            System.out.println("Total de la forma1:"+myMatriz.getSumaTotal());
            
            //Forma 2:POLIMORFISMO
            IMatriz i_matriz=myMatriz;
            System.out.println("Total de la forma2:"+i_matriz.getSumaTotal());
            
            //Forma 3:CASTING DE LA INTERFACE
            
            int total=((IMatriz)myMatriz).getSumaTotal();
            System.out.println("Total de la forma3:"+total);
            
            
            Par_Numero par=new Par_Numero(4,5);
            //Forma2:
            IMatriz i3=par;
            System.out.println("Total de la forma2:"+i3.getSumaTotal());
            
            
            VectorNumero vector2=new VectorNumero(5);
            //Forma2:
            IMatriz i4=vector2;
            System.out.println("Total de la forma2:"+i4.getSumaTotal());
           
            
        } catch (Exception ex) {
            System.err.println("Error:"+ex.getMessage());
        }
    }
}